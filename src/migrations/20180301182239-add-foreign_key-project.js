

module.exports = {
  up: (queryInterface, Sequelize) => {
    queryInterface.addColumn('Project', 'categoryId', {
      type: Sequelize.INTEGER,
      onDelete: 'CASCADE',
      allowNull: false,
      references: {
        model: 'Category',
        key: 'id',
      },
    });
  },

  down: (queryInterface, Sequelize) => {
  },
};
