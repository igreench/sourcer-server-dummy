
module.exports = (sequelize, DataTypes) => {
  const Project = sequelize.define('Project', {
    detail: DataTypes.TEXT,
  }, {});
  Project.associate = function (models) {
    models.Project.belongsTo(models.User, {
      onDelete: 'CASCADE',
    });
    models.Project.belongsTo(models.Category, {
      onDelete: 'CASCADE',
    });
    models.Project.belongsToMany(models.User, {
      through: {
        model: models.UserProjectResponse,
        unique: false,
        otherKey: 'projectId',
      },
    });
  };
  return Project;
};
