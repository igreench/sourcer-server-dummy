module.exports = (sequelize, DataTypes) => {
  const Profile = sequelize.define('Profile', {
    firstName: DataTypes.STRING,
    surname: DataTypes.STRING,
    lastName: DataTypes.STRING,
    phone: DataTypes.STRING,
    contacts: DataTypes.STRING,
    aboutMe: DataTypes.TEXT,
  });

  Profile.associate = (models) => {
    models.Profile.belongsTo(models.User, {
      onDelete: 'CASCADE',
    });
  };

  return Profile;
};
