const project = require('../controllers/controller').projectController;


module.exports = (app) => {
  app.get('/api', (req, res) => res.status(200).send({
    message: 'Welcome to the Todos API!',
  }));

  // app.post('/api/project', project.create);
  app.get('/api/projects', project.list);
  // app.get('/api/project/:todoId', project.retrieve);
  // app.put('/api/project/:todoId', project.update);
  // app.delete('/api/project/:todoId', project.destroy);

  app.all('/api/todos/:todoId/items', (req, res) => res.status(405).send({
    message: 'Method Not Allowed',
  }));
};
