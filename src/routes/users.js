module.exports = (app) => {
  app.post('/api/user/feedback', (req, res) => {
    res.send({
      success: true,
      payload: {
        message: 'POST /api/user/feedback',
      },
    });
  });

  app.put('/api/user/feedback', (req, res) => {
    res.send({
      success: true,
      payload: {
        message: 'PUT /api/user/feedback',
      },
    });
  });

  app.get('/api/user/notifications', (req, res) => {
    res.send({
      success: true,
      payload: {
        message: 'GET /api/user/notifications',
      },
    });
  });
};
